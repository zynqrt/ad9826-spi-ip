`timescale 1 ns / 1 ps

module AD9826Spi #
	(
		parameter integer C_ADC_ADDRESS_SIZE = 3,
		parameter integer C_ADC_DATA_SIZE = 9,
		parameter integer C_REGISTER_CR_SIZE = 9
	)
	(
	input aclk,
	input aresetn,
	
	input [C_REGISTER_CR_SIZE-1:0] reg_cr,
	input reg_cr_strobe,

	input [C_ADC_ADDRESS_SIZE+C_ADC_DATA_SIZE-1:0] reg_wr,
	input reg_wr_strobe,

	input [C_ADC_ADDRESS_SIZE-1:0] reg_rdaddr,
	input reg_rdaddr_strobe,

	output [C_ADC_ADDRESS_SIZE-1:0] address,
	output [C_ADC_DATA_SIZE-1:0] data_out,
	output ready,

	output wire SCLK,
	output wire SLOAD,
	output wire SDATA_o,
	input  wire SDATA_i,
	output wire SDATA_t
);

wire [7:0] prescaller = reg_cr[7:0];
wire enable = reg_cr[8];


bit [7:0] psc_cnt;
wire psc_cnt_equal = (psc_cnt == prescaller);
bit [5:0] bit_cnt;

bit sclk;
bit sload;
bit sdata_t;

bit [15:0] shift_reg;
bit [2:0] address_reg;

	enum bit [3:0] {
		IDLE,
		LOAD_WR,
		WRITE,
		
		LOAD_RD,
		READ
	} state;

	always_ff @(posedge aclk) begin
		if (~aresetn) begin
			state <= IDLE;
			sclk <= 0;
			sload <= 1;
			bit_cnt <= 0;
			sdata_t <= 0;
			address_reg <= 0;
		end
		else begin
			case (state)
				IDLE: begin
					sclk <= 0;
					sload <= 1;
					bit_cnt <= 0;
					sdata_t <= 0;

					if (reg_wr_strobe & enable) begin
						sload <= 0;
						state <= LOAD_WR;
						sdata_t <= 0;
					end

					if (reg_rdaddr_strobe & enable) begin
						sload <= 0;
						state <= LOAD_RD;
						sdata_t <= 0;
					end
				end
				
				LOAD_WR: begin
					shift_reg <= {1'b0, reg_wr[11:9], 3'b000, reg_wr[8:0]};
					state <= WRITE;
				end

				WRITE: begin
					if (bit_cnt == 6'd32) begin
						state <= IDLE;
					end
				end

				LOAD_RD: begin
					address_reg <= reg_rdaddr[2:0];
					shift_reg <= {1'b1, reg_rdaddr[2:0], 12'd0};
					state <= READ;
				end

				READ: begin
					if (bit_cnt == 6'd14) begin
						sdata_t <= 1'b1; // Переключиться на вход
					end

					if (bit_cnt == 6'd32) begin
						state <= IDLE;
					end
				end
			endcase

			if (psc_cnt_equal & sclk) begin
				shift_reg <= {shift_reg[14:0], 1'b0};
			end

			if (psc_cnt == prescaller) begin
				sclk <= ~sclk;
				bit_cnt <= bit_cnt + 1;
			end
		end
	end

	wire read_strobe = (state == READ) & sdata_t & psc_cnt_equal & (~sclk);

bit [8:0] read_reg;
	always_ff @(posedge aclk) begin
		if (~aresetn) begin
			read_reg <= 0;
		end
		else begin
			case (state)
				LOAD_RD: read_reg <= 0;
				READ: if (read_strobe) read_reg <= {read_reg[8:0], SDATA_i};
				default: read_reg <= read_reg;
			endcase
		end
	end

	always_ff @(posedge aclk) begin
		if (~aresetn) begin
			psc_cnt <= 0;
		end
		else begin
			if (state == IDLE) begin
				psc_cnt <= 0;
			end
			else if ((state == WRITE) | (state == READ)) begin
				psc_cnt <= psc_cnt + 1;
				if (psc_cnt_equal)
					psc_cnt <= 0;
			end
		end
	end

	assign SDATA_o = shift_reg[15];
	assign SCLK = sclk & enable;
	assign SLOAD = sload;
	assign SDATA_t = sdata_t;

	assign address = address_reg;
	assign data_out = read_reg;
	assign ready = sload;
endmodule
