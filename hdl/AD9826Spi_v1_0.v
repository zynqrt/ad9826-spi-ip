
`timescale 1 ns / 1 ps

	module AD9826Spi_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here
		output wire SCLK,
		output wire SLOAD,
		output wire SDATA_o,
		input  wire SDATA_i,
		output wire SDATA_t,	
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);

	localparam integer C_ADC_ADDRESS_SIZE = 3;
	localparam integer C_ADC_DATA_SIZE = 9;
	localparam integer C_REGISTER_CR_SIZE = 9;

	wire [C_REGISTER_CR_SIZE-1:0] REG_CR;
	wire REG_CR_STROBE;
	wire [C_ADC_ADDRESS_SIZE+C_ADC_DATA_SIZE-1:0] REG_WR;
	wire REG_WR_STROBE;
	wire [C_ADC_ADDRESS_SIZE-1:0] REG_RDADDR;
	wire REG_RDADDR_STROBE;

	wire [C_ADC_ADDRESS_SIZE-1:0] address;
	wire [C_ADC_DATA_SIZE-1:0] data_out;
	wire ready;

// Instantiation of Axi Bus Interface S00_AXI
	AD9826Spi_v1_0_S00_AXI # ( 
		.C_ADC_ADDRESS_SIZE(C_ADC_ADDRESS_SIZE),
		.C_ADC_DATA_SIZE   (C_ADC_DATA_SIZE),
		.C_REGISTER_CR_SIZE(C_REGISTER_CR_SIZE),

		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) AD9826Spi_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),

		.REG_CR           (REG_CR),
		.REG_CR_STROBE    (REG_CR_STROBE),
		.REG_WR           (REG_WR),
		.REG_WR_STROBE    (REG_WR_STROBE),
		.REG_RDADDR       (REG_RDADDR),
		.REG_RDADDR_STROBE(REG_RDADDR_STROBE),
		.REG_STATUS       ({ready, address, data_out})
	);

	// Add user logic here
	AD9826Spi # (
		.C_ADC_ADDRESS_SIZE(C_ADC_ADDRESS_SIZE),
		.C_ADC_DATA_SIZE   (C_ADC_DATA_SIZE),
		.C_REGISTER_CR_SIZE(C_REGISTER_CR_SIZE)
	) dd (
		.aclk             (s00_axi_aclk),
		.aresetn          (s00_axi_aresetn),
		.reg_cr           (REG_CR),
		.reg_cr_strobe    (REG_CR_STROBE),
		.reg_wr           (REG_WR),
		.reg_wr_strobe    (REG_WR_STROBE),
		.reg_rdaddr       (REG_RDADDR),
		.reg_rdaddr_strobe(REG_RDADDR_STROBE),
		
		.address          (address),
		.data_out         (data_out),
		.ready            (ready),

		.SCLK             (SCLK),
		.SLOAD            (SLOAD),
		.SDATA_o          (SDATA_o),
		.SDATA_i          (SDATA_i),
		.SDATA_t          (SDATA_t)
	);
	// User logic ends

	endmodule
